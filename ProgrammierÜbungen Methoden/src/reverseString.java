import java.util.*;

public class reverseString {

	public static void main(String[] args) {
		
		int x;
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Bitte geben sie einen Satz ein der umgekehrt werden soll");
		String eingabe = sc.next();
		
		System.out.println("Ihre Eingabe: " + eingabe);

		char[] buchstabenliste = eingabe.toCharArray();
		
		// Initalisieren der Liste mit bekannter L�nge
		char[] umgekehrteListe = new char[buchstabenliste.length];
		
		for(int i = 0; i < buchstabenliste.length; i++) {
			if(i == 0) {
				x = 1;
			} else {
				x = 0;
			}
			umgekehrteListe[i] = buchstabenliste[(buchstabenliste.length - x)-i];
		}
		
		String ergebnis = new String(umgekehrteListe);
		
		System.out.println("Ergebnis: " + ergebnis);
		
	

	}

}
