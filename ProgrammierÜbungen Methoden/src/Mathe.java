import java.util.Scanner;
import java.math.*;

public class Mathe {

	public static void main(String[] args) {
		Scanner meinScanner = new Scanner(System.in); 
		System.out.println("Bitte geben Sie eine Zahl ein: ");
		double x = meinScanner.nextDouble();
		
		System.out.println("Bitte geben Sie Kathete1 ein: ");
		double k1 = meinScanner.nextDouble();
		System.out.println("Bitte geben Sie Kathete2 ein: ");
		double k2 = meinScanner.nextDouble();
		
		
		double meinQuadrat = quadrat(x);
		double meineHypotenuse = hypotenuse(k1, k2);
		
		
		System.out.println("Das Quadrat von " + x + " ist: " + meinQuadrat); 
		System.out.println("Die L�nge der Hypotenuse aus " + k1 + " und " + k2 + " ist: " + meineHypotenuse); 


	}

	public static double quadrat(double x) {
		return x*x;
	}
	
	public static double hypotenuse(double kathete1, double kathete2) {
		return Math.sqrt(quadrat(kathete1) + kathete2);
	}
}
