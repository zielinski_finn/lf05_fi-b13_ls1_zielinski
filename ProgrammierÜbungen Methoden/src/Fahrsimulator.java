
public class Fahrsimulator {

	public static void main(String[] args) {
		// v = Geschwindigkeit 0-130
		// dv positiver Wert = Beschleunigen, negativ = bremsen
		double v = 0.0;
		double dv = 12.0;
		
		v = beschleunige(v,dv);
		
		System.out.println(v);

	}
	
	public static double beschleunige(double v, double dv) {
		// Man k�nnte die funktion mit einer for() Schleife laufen lassen und jedes mal nach
		// einer erneuten Eingabe fragen aber das war nicht die Aufgabe
		if(dv >= 1 && v<130) {
			v++;
		} else if(dv <=0 && v>0) {
			v--;
		}
		
		return v;
	}

}
