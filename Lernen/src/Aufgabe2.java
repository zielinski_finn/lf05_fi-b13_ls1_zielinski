import java.util.*;

public class Aufgabe2 {

	public static void main(String[] args) {
	
		Scanner meinScanner = new Scanner(System.in);
		
		System.out.println("Hallo Benutzer! Wie alt bist du? ");
		short alter = meinScanner.nextShort();
		
		System.out.println("Wie ist dein Name?");
		String name = meinScanner.next();
		
		System.out.println("Hallo " + name + " du bist " + alter + " Jahre alt");
		
		meinScanner.close();
	}

}
