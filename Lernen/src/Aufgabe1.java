import java.util.Scanner; // Import der Klasse Scanner
import java.math.*; // Import der Klasse math

public class Aufgabe1 {

	public static void main(String[] args) {

		// Neues Scanner-Objekt myScanner wird erstellt
		Scanner myScanner = new Scanner(System.in);

		System.out.print("Bitte geben Sie eine ganze Zahl ein: ");

		// Die Variable zahl1 speichert die erste Eingabe
		int zahl1 = myScanner.nextInt();

		System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");

		// Die Variable zahl2 speichert die zweite Eingabe
		int zahl2 = myScanner.nextInt();

		// Die Addition der Variablen zahl1 und zahl2
		// wird der Variable ergebnis zugewiesen.
		int ergebnis = zahl1 + zahl2;

		// Subtraktion
		int subtraktionsErgebnis = zahl1 - zahl2;
		
		// multiplukationsErgebnis
		int multiplikationsErgebnis = zahl1 * zahl2;
		
		// dividierungsErgebnis
		int dividierungsErgebnis = zahl1 / zahl2;

		System.out.print("\n\n\nErgebnis der Addition lautet: ");
		System.out.print(zahl1 + " + " + zahl2 + " = " + ergebnis);

		System.out.print("\n\n\nErgebnis der Subtraktion lautet: ");
		System.out.print(zahl1 + " - " + zahl2 + " = " + subtraktionsErgebnis);
		
		System.out.print("\n\n\nErgebnis der Multiplikation lautet: ");
		System.out.print(zahl1 + " * " + zahl2 + " = " + multiplikationsErgebnis);
		
		System.out.print("\n\n\nErgebnis der Dividierung lautet: ");
		System.out.print(zahl1 + " / " + zahl2 + " = " + dividierungsErgebnis +"\n");
		System.out.print("Dividierung Fehlerhaft, integer benutzt \n");
		
		System.out.print("Korrektur: \n");
		float a = zahl1, b = zahl2;
		
		System.out.println(a/b);
		
		myScanner.close();
	}

}
