
public class Treppe {

	public static void main(String[] args) {
		// Treppenaufgabe

		// Treppenhöhe
		int h = 4;

		// Stufenbreite
		int b = 3;

		for(int i = 0; i <= (h*b); i++) {
			
			// Leerzeichen für Breite
			for (int a = 0; a < (h * b); a++) {
				System.out.print(" ");
			}

			// Stufenprint
			for (int x = 0; x < b; x++) {
				System.out.print("*");
				
				for(int t= 0; t<i; t++) {
					System.out.print("*");
				}
			}
			
			// Zeilenumbruch und Höhenveränderung
			System.out.print("\n");
			h--;
		}
		
		

	}

}
