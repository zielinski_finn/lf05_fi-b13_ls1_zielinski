import java.util.*;

public class AuswahlstrukturenAufgabe1 {

	public static void main(String[] args) {
		/*
		 * Erstellen Sie folgende Programme. 
		 * Es werden jeweils 2 Zahlen eingegeben: 1.
		 * Nennen Sie Wenn-Dann-Aktivit�ten aus ihrem Alltag. 2. Wenn beide Zahlen
		 * gleich sind, soll eine Meldung ausgegeben werden (If) 3. Wenn die 2. Zahl
		 * gr��er als die 1. Zahl ist, soll eine Meldung ausgegeben werden (If) 4. Wenn
		 * die 1. Zahl gr��er oder gleich als die 2. Zahl ist, soll eine Meldung
		 * ausgegeben werden, ansonsten eine andere Meldung (If-Else) Erstellen Sie
		 * folgende Programme. Es werden jeweils 3 Zahlen eingegeben: 1. Wenn die 1.Zahl
		 * gr��er als die 2.Zahl und die 3. Zahl ist, soll eine Meldung ausgegeben
		 * werden (If mit && / Und) 2. Wenn die 3.Zahl gr��er als die 2.Zahl oder die 1.
		 * Zahl ist, soll eine Meldung ausgegeben werden (If mit || / Oder) 3. Geben Sie
		 * die gr��te der 3 Zahlen aus. (If-Else mit && / Und)
		 */
		
		
		//Aufgabe 1 Teil 1
		Scanner schreib = new Scanner(System.in);
				
		System.out.println("Bitte geben Sie eine Zahl ein: ");
		int zahl1 = schreib.nextInt();
		System.out.println("Bitte geben Sie eine zweite Zahl ein: ");
		int zahl2 = schreib.nextInt();
		
		System.out.println("--------------------------------------");
		System.out.println("----------------Ausgabe---------------");
		System.out.println("--------------------------------------");
		
		if(zahl1 < zahl2) {
			System.out.println(zahl1 + " ist < als " + zahl2 +" (zahl2)");
		} else if(zahl1 > zahl2){
			System.out.println(zahl2 + " ist < als "+  zahl1 +" (zahl1)");
		} else if(zahl1 == zahl2) {
			System.out.println("Beide Zahlen sind gleich gro�");
		}

		if(zahl1 >= zahl2) {
			System.out.println("Zahl 1 ist gr��ergleich Zahl2");
		}else {
			System.out.println("Zahl 1 ist NICHT gr��ergleich Zahl2");
		}
		
		// Teilaufgabe 2
		System.out.println("Bitte geben Sie eine Zahl ein: ");
		zahl1 = schreib.nextInt();
		System.out.println("Bitte geben Sie eine zweite Zahl ein: ");
		zahl2 = schreib.nextInt();
		System.out.println("Bitte geben Sie eine dritte Zahl ein: ");
		int zahl3 = schreib.nextInt();
		
		if(zahl1 > zahl2 && zahl1 > zahl3) {
			System.out.println("Zahl1 ist nicht gr��er als Zahl2 und Zahl3");
		}
		
		if(zahl3 > zahl2 || zahl3 > zahl1) {
			System.out.println("Zahl3 ist gr��er als Zahl2 oder Zahl1");
		}
		
		if(zahl3 > zahl2 && zahl3 > zahl1) {
			System.out.println("Zahl3 ist gr��er als Zahl2 und Zahl1");
		} else if (zahl2 > zahl1 && zahl2 > zahl3) {
			System.out.println("Zahl2 ist gr��er als Zahl1 und Zahl3");
		} else if (zahl1 > zahl3 && zahl1 > zahl2) {
			System.out.println("Zahl1 ist gr��er als Zahl2 und Zahl3");
		}
	}
	

}
