import java.util.*;

public class finntest {

	public static void main(String[] args) {
		// Datentypen
		boolean meinWahrheitsWert = true;
		char meinZeichen = 'c';
		byte meineMiniZahl = 127;
		short meineKurzeZahl = 5000;
		int meineZahl = 50000000;
		long meineRiesenZahl = 500000000;
		float meineKommaZahl = 50.0f;
		double meineSuperKommaZahl = 500.50;
		String einleitung = "Hallo, das ist ein Programm";
		
		final double PI = 3.1415926535897932385;
		final byte MWST = 19;
		
		
		// Definierungsmöglichkeiten
		byte a, b, c = 20, d = 30, e;
		meinZeichen = 'C';
		
		// Scanner
		Scanner meinScanner = new Scanner(System.in);
		System.out.println("Wie lt sind Sie?");
		//short alter = meinScanner.nextShort();
		short alter = 100;
		System.out.println(" Sie sind " + alter + " Jahre alt.");
		
		// Wiederholung Formartierung
		System.out.println("Hallo Welt \"Hallo Programm!\"");
		
		String coolerText = "Hallo dieser Text ist cool!";
		System.out.printf("|%40s|\n", coolerText);
		
		double dd = 5000.100;
		System.out.printf("|%15.2f|", dd);
		
		
	}

}
