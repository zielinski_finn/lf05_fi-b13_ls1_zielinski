import java.util.*;

public class FallunterscheidungenAufgabe1 {

	public static void main(String[] args) {
		// NOTEN
		
		Scanner f = new Scanner(System.in);
		
		System.out.println("Bitte Note als Ganzzahl eingeben: ");
		int eingabe = f.nextInt();
		
		// Kann auch mit switch-cases gel�st werden
		if(eingabe == 1) {
			System.out.println("Sehr gut");
		} else if(eingabe == 2) {
			System.out.println("Gut");
		} else if(eingabe == 3) {
			System.out.println("Befriedigend");
		} else if(eingabe == 4) {
			System.out.println("Ausreichend");
		} else if(eingabe == 5) {
			System.out.println("Mangelhaft");
		} else if(eingabe == 6) {
			System.out.println("Ungen�gend");
		} else {
			System.out.println("Eingabe nicht as Note erkannt. Nur Zahlen zwischen 1-6");
		}
	}

}
