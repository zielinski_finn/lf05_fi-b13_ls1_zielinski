import java.util.*;

public class SchleifenAufgabe1_2_3 {

	public static void main(String[] args) {
		// Aufgabe 1, Z�hlen
		Scanner f = new Scanner(System.in);
		
		System.out.println("Bitte Programm '1' oder Programm '2' ausw�hlen");
		System.out.println("'1' = Von 1 bis N");
		System.out.println("'2' = Von N bis 1");

		int option = f.nextInt();	
		
		if(option == 1) {
			// Ausgabe von 1 bis n
			System.out.println("Ausgew�hlt: Ausgabe von 1 bis n. Bitte definieren Sie n: ");
			int n = f.nextInt();
			
			for(int i=0; i <= n; i++) {
				System.out.print(i);
				if(i < n) {
					System.out.printf(", ");
				}
			}
			
			System.out.println("");
		} else if(option == 2) {
			// Ausgabe von n bis 1
			System.out.println("\n Ausgew�hlt: Ausgabe von n bis 1. Bitte definieren Sie n: ");
			int n = f.nextInt();
			
			// Ein Bisschen 'Hacky' gel�st :
			int iteration = 0;
			
			for(int i=0; i < n; i++) {
				System.out.print(n-iteration);
				iteration++;
				if(iteration < n) {
					System.out.printf(", ");
				}
			}
			
		} else {
			System.out.println("Fehler! Falsche Eingabe, bitte nur 1 oder 2 eingeben");
		}
		
	
		// Aufgabe 2
		
		
		// Aufgabe 3
		System.out.println("\n---------");
		System.out.println("Aufgabe 3");
		System.out.println("---------");
		
		int ende = 200;
		
		for(int zahl = 1; zahl<ende; zahl++) {
			if(zahl % 7 == 0) {
				System.out.println(zahl + " ist teilbar durch 7 ohne restwert");
			}
			if(zahl % 5 != 0) {
				// System.out.println(zahl + " ist nicht teilbar durch 5");
				if(zahl % 4 == 0) {
					System.out.println(zahl + " ist teilbar durch 4, aber nicht durch 5");
				}
			}
			
		}

	}

}