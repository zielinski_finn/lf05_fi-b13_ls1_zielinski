import java.util.*;

// kgV ung ggT Berechnung
public class ZusatzAufgabe1 {

	public static void main(String[] args) {
		// Deklaration
		Scanner f = new Scanner(System.in);
		int a, b, rechnungA1, rechnungB1;

		while (true) {
			// Werteingabe
			System.out.println("Bitte geben Sie eine Ganzzahl ein: ");
			a = f.nextInt();
			rechnungA1 = a;
			System.out.println("Bitte geben Sie eine zweite Ganzzahl ein: ");
			b = f.nextInt();
			rechnungB1 = b;

			// Berechnung KGV
			System.out.println("KGV und GGT wird berechnet...");

			while (rechnungA1 != rechnungB1) {
				if (rechnungA1 < rechnungB1) {
					rechnungA1 += a;
				} else {
					rechnungB1 += b;
				}
			}
			
			// Berechnung ggT
			System.out.println("KGV: " + rechnungA1);
			
			rechnungA1 = a;
			rechnungB1 = b;
			while (rechnungB1 != 0) {
				if (rechnungA1 > rechnungB1) {
					rechnungA1 = rechnungA1 - rechnungB1;
				} else {
					rechnungB1 = rechnungB1 - rechnungA1;
				}
			}

			System.out.println("GGT: " + rechnungA1);

		}

	}

}
