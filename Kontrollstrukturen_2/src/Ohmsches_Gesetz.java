import java.util.*;

public class Ohmsches_Gesetz {

	public static void main(String[] args) {
		// Deklaration
		Scanner f = new Scanner(System.in);
		String eingabe1;
		double eingabe2, eingabe3;

		// Eingabe
		System.out.println("Bitte geben Sie ein g�ltiges Zeichen der Ohmschen Regeln ein, um dieses zu berechnen. "
				+ "Gro�- und Kleinschreibung egal.");
		eingabe1 = f.next();

		// Fallunterscheidung
		switch (eingabe1.toUpperCase()) {
		case "R":
			// R = U / I
			System.out.println("Berechnung von R. Bitte geben Sie U ein: ");
			eingabe2 = f.nextDouble();
			System.out.println("Bitte geben Sie I ein: ");
			eingabe3 = f.nextDouble();
			
			System.out.println("R = " + (eingabe2 / eingabe3));
			
			break;
		case "U":
			// U = R * I
			System.out.println("Berechnung von U. Bitte geben Sie R ein: ");
			eingabe2 = f.nextDouble();
			System.out.println("Bitte geben Sie I ein: ");
			eingabe3 = f.nextDouble();
			
			System.out.println("U = " + (eingabe2 * eingabe3));
			
			break;
		case "I":
			// I = U / R
			System.out.println("Berechnung von I. Bitte geben Sie U ein: ");
			eingabe2 = f.nextDouble();
			System.out.println("Bitte geben Sie R ein: ");
			eingabe3 = f.nextDouble();
			
			System.out.println("I = " + (eingabe2 / eingabe3));

			break;
		default:
			System.out.println("Fehlerhafte Eingabe, bitte erneut Versuchen");
			break;
		}

	}

}
