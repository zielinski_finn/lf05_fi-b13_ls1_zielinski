import java.util.*;

public class BMI {

	public static void main(String[] args) {
		// Deklaration der Variablen
		Scanner fs = new Scanner(System.in);
		double groe�e, bmi, gewicht;
		String geschlecht;

		// Eingabe und Zuweisung
		System.out.println("Bitte geben Sie Ihr Gewicht in KG (Ganzzahl, aufgerundet) an: ");
		gewicht = fs.nextDouble();
		System.out.println("Bitte geben Sie Ihre Gr��e in cm an: ");
		groe�e = fs.nextDouble();
		System.out.println("Bitte geben Sie Ihr biologisches Geschlecht (m/w) an: ");
		geschlecht = fs.next();

		// In cm in Meter umwandeln
		double meter = groe�e / 100;

		// BMI berechnen
		bmi = gewicht / (meter * 2);
		
		// Ergebnisausgabe
		if (geschlecht == "m") {

			if (bmi < 20.0) {
				System.out.println("Untergewicht! BMI = " + bmi);
			} else if (bmi >= 20.0 && bmi <= 25.0) {
				System.out.println("Normalgewicht. BMI = " + bmi);
			} else {
				System.out.println("�bergewicht! BMI = " + bmi);
			}

		} else {

			if (bmi < 19.0) {
				System.out.println("Untergewicht! BMI = " + bmi);
			} else if (bmi >= 19.0 && bmi <= 24.0) {
				System.out.println("Normalgewicht. BMI = " + bmi);
			} else {
				System.out.println("�bergewicht! BMI = " + bmi);
			}
		}

	}

}
