import java.util.*;

public class Matrix {

	public static void main(String[] args) {
		// Multiplikationsmatrix
		// 10 Zeilen / 10 Spalten
		// zwischen 2 und 9 werden alle Zahlen durch * gekennzeichnet
		Scanner f = new Scanner(System.in);
		int eingabe, x = 0;

		// Eingabe der Zahl, nur zwischen 2 und 9
		System.out.println("Bitte geben Sie eine Zahl zwischen 2 und 9 ein!");
		eingabe = f.nextInt();

		while (eingabe < 2 || eingabe > 9) {
			System.out.println("Fehler! Nur Zahlen zwischen 2 und 9!");
			System.out.println("Bitte geben Sie eine neue Zahl ein: ");
			eingabe = f.nextInt();
		}

		System.out.println("Matrix wird erstellt...");
		System.out.print(" ");

		while (x < 100) {
			
			
			if (x % 10 == 0 && x != 0) {
				System.out.println("");
			}

			if (x % eingabe == 0 && x != 0) {
				System.out.print(" * ");
			} else {
				System.out.print(x + " ");
			}
			
			x++;
		}

	}

}
