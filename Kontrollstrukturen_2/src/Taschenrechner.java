import java.util.*;

public class Taschenrechner {

	public static void main(String[] args) {
		// Deklaration
		Scanner f = new Scanner(System.in);
		double eingabe1, eingabe2;
		String eingabe3; 
		// Hardcoded Methoden
		char[] methoden = {'+', '-', '*', '/'};
		
		// Eingabe von den 2 Werten
		System.out.println("Bitte Wert 1 eingeben: ");
		eingabe1 = f.nextDouble();
		
		System.out.println("Bitte Wert 2 eingeben: ");
		eingabe2 = f.nextDouble();
		
		// Auswahl der Rechenmethode: +, -, *, /
		System.out.println("Bitte eine der "+ methoden.length +" Rechenmethode ausw�hlen: ");
		for(int i = 0; i < methoden.length; i++) {
			System.out.println(methoden[i]);
		}
		eingabe3 = f.next();
		
		// Fallunterscheidung
		switch(eingabe3) {
		case "+":
			System.out.println("Ergebnis: " + (eingabe1 + eingabe2));
			break;
		case "-":
			System.out.println("Ergebnis: " + (eingabe1 - eingabe2));
			break;
		case "*":
			System.out.println("Ergebnis: " + (eingabe1 * eingabe2));
			break;
		case "/":
			System.out.println("Ergebnis: " + (eingabe1 / eingabe2));
			break;
		default:
			// Jetzt so gel�st, h�tte man aber auch solange in Endlosschleife laufen lassen k�nnen bis eine richtige Eingabe 
			// erkannt worden ist, aber nicht die Aufgabe.
			System.out.println("Kein g�ltiges Zeichen erkannt, bitte erneut probieren");
			break;
		}
		
		
		
		
		
	}

}
