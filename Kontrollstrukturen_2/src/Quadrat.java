import java.util.*;

public class Quadrat {

	public static void main(String[] args) {
		// Deklaration
		Scanner f = new Scanner(System.in);
		int seitenLaenge;
		
		// Eingabe
		System.out.println("Bitte geben Sie eine gew�nschte Seitenl�nge f�r ein Quadrat an (Ganzzahlen): ");
		seitenLaenge = f.nextInt();
		
		// Beginn des Quadrats ausgeben
		for(int i = 0; i < seitenLaenge; i++) {
			System.out.print("*");
		}
		
		System.out.println("");
		// K�rper des Quadrats ausgeben
		for(int i = 1; i <= seitenLaenge-2; i++) {
			for(int j = 0; j < seitenLaenge; j++) {
				if(j == 0) {
					System.out.print("*");
				} else if(j == seitenLaenge -1){
					System.out.print("*\n");
				} else {
					System.out.print(" ");
				}
			}
		}
		
		// Ende des Quadrats ausgeben
		if(seitenLaenge == 1) {
			System.out.print("");
		} else {
			for(int i = 0; i < seitenLaenge; i++) {
				System.out.print("*");
			}
		}
	}

}
