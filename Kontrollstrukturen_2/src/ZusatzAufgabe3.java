import java.util.Scanner;

public class ZusatzAufgabe3 {

	public static void main(String[] args) {
		// Ein Array kann mit:
		// System.out.println(java.util.Arrays.toString(ARRAYNAME));
		// Problemlos ausgegeben werden (Notiz fuer mich)

		// Variablen Definition
		Scanner f = new Scanner(System.in);
		int n;
		String option;

		do {
			System.out.print("Geben Sie die Genauigkeit N in einer Ganzzahl an: ");
			n = f.nextInt();

			// berechnung ln2 = 1 - 1/2 + 1/3 - 1/4 + ...
			System.out.println("Berechnung von ln2 mit der Genauigkeit " + n);
			System.out.println("...");

			log2A(n);
			log2B(n);

			// Erneut ausf�hren oder Zufrieden?
			System.out.println("\nErneute Berechnung mit neuen Zahlen? (j/n): ");
			option = f.next();

		} while(option.contains("J") || option.contains("j"));

		System.out.println(">>> Programm beendet <<<");
	}

	public static void log2A(int n) {
		// Liste etwas gr��er gestalten
		double[] liste = new double[n + 2];

		// Array mit den Zahlenwerten der Br�che f�llen Bruch 1/2 = 0,5, ...
		for (int i = 2; i <= n + 2; i++) {
			liste[i - 2] = (1.0 / (double) i);
		}

		// Ergebnis = Abwechselnd die Br�cke + und - rechnen
		for (int i = 0; i < liste.length - 1; i++) {
			if (i + 2 % 2 == 0 || i % 2 == 0) {
				// Addition, nur gerader i Wert wird addiert
				liste[i + 1] = liste[i] + liste[i + 1];
				liste[i] = 0;

			} else {
				// Subtraktion, nur ungerader i Wert wird subtrahiert
				liste[i + 1] = liste[i] - liste[i + 1];
				liste[i] = 0;
			}
		}

		// Ausgabe: Ergebnis
		System.out.println("Aufgabe a.) ln2 ist c.a.: " + liste[liste.length - 1]);
	}

	public static void log2B(int n) {
		// F�r Aufgabe b.) habe ich mir �berlegt einen neuen Abschnitt zu machen
		// und dort mit 2 unterschiedlichen Arrays zu arbeiten
		// arrayPlus = alle Werte die + gerechnet werden
		// arrayMinus = alle Werte die - gerechnet werden

		// Arrayerstellung von den Werten die addiert werden
		double[] arrayPlus = new double[(int) (((double) n) / 2.0)];
		for (int i = 2; i <= arrayPlus.length; i++) {
			if (i % 2 == 0) {
				arrayPlus[i - 2] = (1.0 / (double) i);
			}
		}

		// Addieren der einzelnen Elemente, da man problemlos mit der 0,0 addieren kann
		// wird einfach jedes Feld miteinander in Reihenfolge addiert
		for (int i = 0; i < arrayPlus.length - 1; i++) {
			arrayPlus[i + 1] = arrayPlus[i] + arrayPlus[i + 1];
			arrayPlus[i] = 0;
		}

		// Arrayerstellung von den Werten die miteinader minus gerechnet werden
		double[] arrayMinus = new double[(int) (((double) n) / 2.0)];
		for (int i = 2; i <= arrayMinus.length; i++) {
			if (i % 2 != 0) {
				arrayMinus[i - 2] = (1.0 / (double) i);
			}
		}

		// Subtraktion jedes Elemts welches nicht 0
		for (int i = 0; i < arrayMinus.length - 1; i++) {
			if (arrayMinus[i] != 0.0) {
				arrayMinus[i + 1] = arrayMinus[i] - arrayMinus[i + 1];
				arrayMinus[i] = 0;
			}
		}

		// die letzten beiden Werte aus den Arrays werden von einander abgezogen
		double ergebnis = arrayPlus[arrayPlus.length - 1] - arrayMinus[arrayMinus.length - 1];

		// Ausgabe des Ergebnis
		System.out.println("Aufgabe b.) ln2 ist c.a.: " + ergebnis);
	}

}
