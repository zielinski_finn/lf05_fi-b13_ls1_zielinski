import java.util.*;

public class Million {

	public static void main(String[] args) {
		// Deklaration
		Scanner f = new Scanner(System.in);
		double startguthaben, zins, zinssumme;
		int jahre = 0;
		final double MILLION = 1000000.0;
		String option;
		
		// Ausführschleife
		do {
			// Eingaben
			System.out.println("Bitte geben Sie Ihr Startguthaben ein: ");
			startguthaben = f.nextDouble();
			System.out.println("Bitte geben Sie den Zinssatz ein: ");
			zins = f.nextDouble();
			
			// Berechnung
			System.out.println("Berechnung beginnt...");
			
			
			while(startguthaben < MILLION) {
				
				// Rechnung: zinssumme += (Startkapital * zinssatz) / 100
				zinssumme = ((startguthaben * zins)/100);

				startguthaben += zinssumme;

				jahre += 1;
				System.out.println("Jahr " + jahre);
				System.out.printf("Guthaben erhöht sich um: + %.2f €\n", zinssumme);
				System.out.printf("Guthaben %.2f €\n", startguthaben);
				
			}
			
			
			// Erneut ausführen oder Zufrieden?
			System.out.println("Erneute Berechnung mit neuen Zahlen? (j/n): ");
			option = f.next();
			
		} while(option.contains("J") || option.contains("j"));
		
		// Nachricht nach beendetem Programm
		System.out.println("Programm beendet. Es dauert c.a. " + jahre + " Jahre bist du Millionär wirst. Kontokosten usw nicht berechnet.");
	}

}
