import java.util.*;

public class Schaltjahr {

	public static void main(String[] args) {
		// Deklaration
		Scanner f = new Scanner(System.in);
		int eingabe;
		
		// Zuweisung
		System.out.println("Bitte geben Sie ein Jahr an: ");
		eingabe = f.nextInt();
		
		// Testen der Eingabe und Ausgabe
		if(eingabe % 4 == 0) {
			if(eingabe % 100 != 0 || eingabe % 400 == 0) {
				System.out.println("Das Jahr '" + eingabe + "' ist ein Schaltjahr.");
			} else {
				System.out.println("Kein Schaltjahr!");
			}
		} else {
			System.out.println("Kein Schaltjahr!");
		}

	}

}
