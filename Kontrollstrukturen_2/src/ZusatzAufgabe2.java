import java.util.*;

public class ZusatzAufgabe2 {

	public static void main(String[] args) {
		// Variablen Definition
		Scanner f = new Scanner(System.in);
		String a;
		boolean test = false;

		// Eingabe
		System.out.println("Bitte geben Sie eine Zahl ein: ");
		a = f.next();

		// Eingabe zu einem Array konvertieren f�r einfache Vergleiche
		char[] arrayA = a.toCharArray();
		
		// Schleife testet nur so lange bis eine doppelte Ziffer gefunden wurde, resourcensparend
		for(int i = 0; i < arrayA.length && test == false; i++) {
			for(int j = (i+1); j < arrayA.length; j++) {
				if(arrayA[i] == arrayA[j]) {
					test = true;
				}	
			}
		}
		
		// Ergebnisausgabe
		if(test == true) {
			System.out.println("Es wurde eine Ziffer doppelt verwendet!");
		} else {
			System.out.println("Es wurde keine Ziffer doppelt verwendet.");
		}
	}

}
