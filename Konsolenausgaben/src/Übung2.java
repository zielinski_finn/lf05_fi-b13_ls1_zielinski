
public class �bung2 {

	public static void main(String[] args) {
		// Aufgabe 1

		String zeichen = "**";

		System.out.printf("%13.2s\n", zeichen);
		System.out.printf("%7.1s", zeichen);
		System.out.printf("%11.1s\n", zeichen);
		System.out.printf("%7.1s", zeichen);
		System.out.printf("%11.1s\n", zeichen);
		System.out.printf("%13.2s\n", zeichen);

		System.out.println("\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");

		// Aufgabe 2

		// Bl�cke = Aufgabe
		System.out.printf("%-5s", "0!");
		System.out.print("=");
		System.out.printf("%-19s", "");
		System.out.printf("=");
		System.out.printf("%4s \n", "1");

		System.out.printf("%-5s", "1!");
		System.out.print("=");
		System.out.printf("%-19s", " 1");
		System.out.printf("=");
		System.out.printf("%4s \n", "1");

		System.out.printf("%-5s", "2!");
		System.out.print("=");
		System.out.printf("%-19s", " 1 * 2");
		System.out.printf("=");
		System.out.printf("%4s \n", "2");

		System.out.printf("%-5s", "3!");
		System.out.print("=");
		System.out.printf("%-19s", " 1 * 2 * 3");
		System.out.printf("=");
		System.out.printf("%4s \n", "6");

		System.out.printf("%-5s", "4!");
		System.out.print("=");
		System.out.printf("%-19s", " 1 * 2 * 3 * 4");
		System.out.printf("=");
		System.out.printf("%4s \n", "24");

		System.out.printf("%-5s", "5!");
		System.out.print("=");
		System.out.printf("%-19s", " 1 * 2 * 3 * 4 * 5");
		System.out.printf("=");
		System.out.printf("%4s \n", "120");

		// Spacer
		System.out.println("\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");

		// AUFGABE 3 Abgeben!

		// Variablen deklarieren, Fahrenheit ist ohne Komma stellen
		int fahr1 = -20;
		int fahr2 = -10;
		int fahr3 = 0;
		int fahr4 = 20;
		int fahr5 = 30;

		double cel1 = -28.8889;
		double cel2 = -23.3333;
		double cel3 = -17.7778;
		double cel4 = -6.6667;
		double cel5 = -1.1111;

		// Ausgabe

		// Spacing f�r die Fahrenheit Spalte
		System.out.printf("%-12s", "Fahrenheit");
		System.out.printf("|");
		// Spacing f�r die Celsius Spalte
		System.out.printf("%10s\n", "Celsius");
		System.out.printf("-----------------------\n");

		// Fahrenheit Ausgabe unformartiert
		System.out.printf("%-12s", fahr1);
		System.out.printf("|");
		// Celsius Ausgabe formartiert, Zeile ohne Umbruch damit die formartierte
		// celsius Zahl "hintendran" geschrieben wird
		System.out.printf("%4s", "");
		System.out.printf("%.2f\n", cel1);

		System.out.printf("%-12s", fahr2);
		System.out.printf("|");
		System.out.printf("%4s", "");
		System.out.printf("%.2f\n", cel2);

		// printf erlaubt nur 2 Argumente weshalb ich manuell ein + vor die positive
		// Zahl schreibe. String + Integer
		System.out.printf("%-12s", "+" + fahr3);
		System.out.printf("|");
		System.out.printf("%4s", "");
		System.out.printf("%.2f\n", cel3);

		System.out.printf("%-12s", "+" + fahr4);
		System.out.printf("|");
		System.out.printf("%5s", "");
		System.out.printf("%.2f\n", cel4);

		System.out.printf("%-12s", "+" + fahr5);
		System.out.printf("|");
		System.out.printf("%5s", "");
		System.out.printf("%.2f\n", cel5);
	}

}
