
public class �bung1 {

	public static void main(String[] args) {
		// Aufgabe 1

		String name = "Finn";
		int alter = 2;
		
		// Teil 1
		System.out.print("Hallo Console, hier ist " + name + ".");
		System.out.println(" Wie gehts dir?");
		
		// Spacer
		System.out.println("--------------------------------------------------");
		
		// Teil 2
		System.out.print("Hallo Console, hier ist " + name + ".\nWie gehts dir?");
		System.out.println(" Console sagt: \"Mir gehts super! Ich bin "+ alter +" Tage alt! \" ");
		
		// Ich bin ein Kommentar!
		
		// Spacer
		System.out.println("--------------------------------------------------");
		
		// Aufgabe 2
		// l�ngste Zeichenreihe aus der �bung, kann mit formatierung abgeschnitten werden.
		String zeichenkette = "*************";
				
		System.out.printf("%13.1s\n", zeichenkette);
		System.out.printf("%14.3s\n", zeichenkette);
		System.out.printf("%15.5s\n", zeichenkette);
		System.out.printf("%16.7s\n", zeichenkette);
		System.out.printf("%17.9s\n", zeichenkette);
		System.out.printf("%18.11s\n", zeichenkette);
		System.out.printf("%19.13s\n", zeichenkette);
		System.out.printf("%14.3s\n", zeichenkette);
		System.out.printf("%14.3s\n", zeichenkette);
		System.out.printf("%14.3s\n", zeichenkette);
		
		// Spacer
		System.out.println("--------------------------------------------------");

		// Aufgabe 3
		double a = 22.4234234;
		double b = 111.2222;
		double c = 4.0;
		double d = 1000000.551;
		double e = 97.34;
		
		System.out.printf( "%.2f\n" , a);
		System.out.printf( "%.2f\n" , b);
		System.out.printf( "%.2f\n" , c);
		System.out.printf( "%.2f\n" , d);
		System.out.printf( "%.2f\n" , e);
		
		

	}

}
