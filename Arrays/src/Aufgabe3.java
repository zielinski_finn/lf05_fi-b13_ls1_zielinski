import java.util.*;

public class Aufgabe3 {

	public static void main(String[] args) {
		// Im Programm �Palindrom� werden �ber die Tastatur 5 Zeichen eingelesen und in
		// einem
		// geeigneten Array gespeichert. Ist dies geschehen, wird der Arrayinhalt in
		// umgekehrter
		// Reihenfolge (also von hinten nach vorn) auf der Konsole ausgegeben.

		Scanner txt = new Scanner(System.in);

		System.out.println("Bitte geben Sie 5 Zeichen ein: ");
		String eingabe = txt.next();
		char[] eingabeZeichen = eingabe.toCharArray();

		System.out.println("Umgekehrte Reihenfolge: ");
		for (int i = eingabeZeichen.length; i <= eingabeZeichen.length && i != 0; i--) {
			System.out.print(eingabeZeichen[i - 1] + "");
		}

	}

}
